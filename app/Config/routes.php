<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
*/
include  'database.php';
$bd = new DATABASE_CONFIG;
$bd = $bd->default;

		
$urls = urldecode(iconv("windows-1251", "UTF-8", $_SERVER['REQUEST_URI']));
$urls = explode('/', $urls);

foreach($urls as $key => $url) {
    if ($url == '') unset($urls[$key]);
};
	
	$pageNum;
	if (is_numeric(end($urls))) {
			$pageNum = array_pop($urls);
		} else {
			$pageNum = 1;
		};

App::import ('Model', 'Page');

$Page = new Page();
$pages = $Page->find('all');

foreach($pages as $page) {
    Router::connect($page['Page']['path'], array('controller' => 'pages', 'action' => 'show', $page['Page']['id']));
}
Router::connect('/', array('controller' => 'pages', 'action' => 'show', 1));
Router::connect('/disc-measuring', array('controller' => 'pages', 'action' => 'constructor'));


$link = mysqli_connect($bd['host'], $bd['login'], $bd['password'], $bd['database']);

mysqli_query($link, "SET NAMES 'utf8'");

function connect($link,$sql){
	 $query = mysqli_query($link, $sql);
        $output;
		if((int)$query->num_rows > 1){
        while ($row = mysqli_fetch_assoc($query)) {
           
                $output[] = $row;  
			}
		}else if((int)$query->num_rows > 0 ){
			while ($row = mysqli_fetch_assoc($query)) {
           
                $output = $row; 
			}
		}else{
			$output = false;
		};
		return $output;
};



if (count($urls) == 1 && $urls[1] !== 'admin' || count($urls) == 2 && $urls[1] !== 'admin') {

    if (count($urls) == 1 && $urls[1] !== 'admin') {
	
		$myUrl =  $urls[1];
		
		$arResult = connect($link,"SELECT url,id FROM `galleries` WHERE url = '$myUrl' limit 1");
            Router::connect('/*', array('controller' => 'galleries', 'action' => 'show', $arResult['id'], $pageNum)); //gallery
        
    };
    if (count($urls) == 2 && $urls[1] !== 'admin') {
		
		$myUrl =  $urls[2];
		
         $arResult = connect($link,"SELECT url,id,gallery_id,weight FROM photos WHERE url = '$myUrl' limit 1");
               
				$currentId = $arResult['id'];
				
				$galleryID = $arResult['gallery_id'];
				
				$currentWeight = $arResult['weight'];
           
			$nextPhoto = connect($link, "SELECT url FROM photos WHERE gallery_id=$galleryID AND published = 1 AND weight > $currentWeight ORDER BY weight ASC LIMIT 1");
			
			$prevPhoto = connect($link,"SELECT url FROM photos WHERE gallery_id=$galleryID AND published = 1 AND weight < $currentWeight ORDER BY weight DESC LIMIT 1");
			
			
			
			if($prevPhoto['url']){
				$_POST["prev_photo"] =  $prevPhoto['url'];
			}
			if($nextPhoto['url']){
				$_POST["next_photo"] = $nextPhoto['url'];
			};
		
				
		
			
            Router::connect('/*', array('controller' => 'photos', 'action' => 'show', $currentId)); //gallery-item
        
    }
};
Router::connect('/admin', array('controller' => 'site', 'action' => 'index', 'admin' => true));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
	