tinymce.init({
    selector: ".edit",
    relative_urls : true,
    relative_urls : true,
	document_base_url : hostname,
	content_css : '/css/style.css',
    convert_urls : false,
    remove_script_host : false,
    plugins: [
        ['advlist anchor autolink autoresize autosave charmap code colorpicker contextmenu directionality'],
        ['emoticons example example_dependency fullscreen hr image importcss insertdatetime layer legacyoutput'],
        ['link lists media nonbreaking noneditable pagebreak paste preview print save searchreplace spellchecker'],
        ['tabfocus table template textcolor textpattern visualblocks visualchars wordcount']
    ],
    language : 'ru',
    importcss_append: true,
    spellchecker_languages: 'Russian=ru,Ukrainian=uk,English=en',
    spellchecker_rpc_url: 'http://speller.yandex.net/services/tinyspell',
    toolbar1: "save preview | cut copy paste pastetext pasteword | undo redo | searchreplace | template | link unlink anchor rc_upload insertdatetime table | hr blockquote charmap emoticons media | spellchecker | visualchars | code fullscreen",
    toolbar2: "fontselect | fontsizeselect | styleselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bold italic underline strikethrough subscript superscript removeformat | numlist bullist | outdent indent ltr rtl"
});