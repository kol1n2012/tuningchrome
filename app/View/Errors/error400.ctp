<?
$this->layout = 'main';
$this->assign('title', 'Ошибка 404 страница не найдена');
?>
<div class="main_side">
    <div class="side_heading line-color">
	<span>Страница не найдена!</span>
	</div>
    <div class="side_tile" align="center">
		<h1>Упс... Error 404.</h1>
		Станица не доступна или была удалена. 
		Чтобы продолжить вы можете перейти, например на <a href="/">главную страницу</a>.
    </div>

</div>
<div class="side_bottom_right"></div>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;