<?php
	$this->layout = 'main';
/**
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 */
?>
<div class="side_heading">
    <span></span>
</div>
<div class="side_tile">
    <div class="error">
    <h2><?php echo $message; ?></h2>
    <p>
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php echo __d('cake', 'An Internal Error Has Occurred.'); ?>
    </p>
	</div>
</div>
<div class="side_bottom_right"></div>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
