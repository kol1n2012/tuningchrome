<?php
$this->assign('title', $page['Page']['seo_title'] ? $page['Page']['seo_title'] : $page['Page']['name']);
$this->assign('description', $page['Page']['seo_description']);
$this->assign('keywords', $page['Page']['seo_keywords']);
$this->assign('name', $page['Page']['name']);
if($page['Page']['path']!=='/index'){
	$this->assign('canonical',$page['Page']['path']);
}
?>
<div class="side_heading line-color">
    <span><?=$page['Page']['name']?></span>
</div>
<div class="side_tile">
    <?=$page['Page']['content']?>
    <div class="remove_sides"></div>
</div>