<h1>Страницы</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
    <li>
		<a href="<?=Router::url(array('action' => 'index'))?>">Страницы</a>
	</li>
	<li class="active">Добавление</li>
</ul>
<div class="well">
	<?=$this->Form->create()?>
	<?=$this->Form->input('name', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('path', array('label' => 'URI: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('content', array('label' => 'Текст: *', 'div' => 'form-group', 'class' => 'form-control edit', 'type' => 'textarea'))?>
    <?=$this->Form->input('seo_title', array('label' => 'SEO title:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_h1', array('label' => 'SEO h1:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_keywords', array('label' => 'SEO keywords:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_description', array('label' => 'SEO description:', 'div' => 'form-group', 'class' => 'form-control'))?>
	<div class="form-actions">
		<?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
	</div>
	<?=$this->Form->end()?>
</div>
