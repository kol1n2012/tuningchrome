<h1>Пользователи</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
    <li>
		<a href="<?=Router::url(array('action' => 'index'))?>">Пользователи</a>
	</li>
	<li class="active">Добавление</li>
</ul>
<div class="well">
	<?=$this->Form->create()?>
	<?=$this->Form->input('email', array('label' => 'Email: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'text'))?>
	<?=$this->Form->input('password', array('label' => 'Пароль: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'password', 'value' => ''))?>
	<?=$this->Form->input('name', array('label' => 'Имя: *', 'div' => 'form-group', 'class' => 'form-control'))?>
	<div class="form-actions">
		<?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
	</div>
	<?=$this->Form->end()?>
</div>
