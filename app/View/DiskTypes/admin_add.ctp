<h1>Марки дисков</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
    <li>
		<a href="<?=Router::url(array('action' => 'index'))?>">Марки дисков</a>
	</li>
	<li class="active">Добавление</li>
</ul>
<div class="well">
	<?=$this->Form->create()?>
	<?=$this->Form->input('type', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
	<div class="form-actions">
		<?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
	</div>
	<?=$this->Form->end()?>
</div>
