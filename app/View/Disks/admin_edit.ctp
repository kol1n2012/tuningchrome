<h1>Диски</h1>
<ul class="breadcrumbs breadcrumb">
    <li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
    </li>
    <li>
        <a href="<?=Router::url(array('action' => 'index'))?>">Диски</a>
    </li>
    <li class="active">Добавление</li>
</ul>
<div class="well">
    <?=$this->Form->create(['type' => 'file'])?>
    <?=$this->Form->input('id')?>
    <?=$this->Form->input('disk_type_id', array('label' => 'Марка диска: *', 'div' => 'form-group', 'class' => 'form-control', 'options' => $disk_types))?>
    <?=$this->Form->input('description', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('pic', array('label' => 'Маленькая картинка: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'file'))?>
    <div class="form-actions">
        <?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
    </div>
    <?=$this->Form->end()?>
</div>
