<h1>Диски</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
	<li class="active"><a href="<?=Router::url(array('action' => 'index'))?>">Диски</a></li>
</ul>
<div class="well form-inline">
    <a class="btn btn-success" href="<?=Router::url(array('action' => 'add'))?>"><i class="fa fa-file"></i> Создать</a>
</div>
<div id="list">

    <ul class="pagination">
        <?php $pages = ceil($count/20); ?>
        <?php $currentPage = isset($this->request->query['page']) ? $this->request->query['page'] : 1 ?>
        <?php for($page = 1; $page <= $pages; $page++): ?>
            <?php if($currentPage == $page): ?>
                <li class="active"><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php else: ?>
                <li><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php endif ?>
        <?php endfor ?>
    </ul>
    <table class="table">
    	<thead>
    		<tr>
    			<th>Марка</th>
                <th>Фото</th>
                <th>Описание</th>
    			<th></th>
    		</tr>
    		<tbody>
    			<?php if($disks): ?>
    			<?php foreach($disks as $item):  $diskType = $item['DiskType']; $item = $item['Disk'];?>
    			<tr>
    				<td><a href="<?=Router::url(['controller' => 'car_types', 'action' => 'edit', $diskType['id']])?>"><?=$diskType['type']?></a></td>
                    <td><?=$item['description']?></td>
                    <td><img width="100" class="thumbnail" src="/flash/<?=$diskType['id']?>/<?=$item['pic']?>"></td>
    				<td>
                        <div class="pull-right">
        					<a class="btn btn-success" href="<?=Router::url(array('action' => 'edit', $item['id']))?>">
        						<i class="fa fa-pencil"></i> Изменить
        					</a>
        					<a class="btn btn-danger delete" href="<?=Router::url(array('action' => 'delete', $item['id']))?>">
        						<i class="fa fa-remove"></i> Удалить
        					</a>
                        </div>
    				</td>
    			</tr>
    			<?php endforeach ?>
    			<?php endif ?>
    		</tbody>
    	</thead>
    </table>
    <ul class="pagination">
        <?php $pages = ceil($count/20); ?>
        <?php $currentPage = isset($this->request->query['page']) ? $this->request->query['page'] : 1 ?>
        <?php for($page = 1; $page <= $pages; $page++): ?>
            <?php if($currentPage == $page): ?>
                <li class="active"><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php else: ?>
                <li><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php endif ?>
        <?php endfor ?>
    </ul>
</div>
