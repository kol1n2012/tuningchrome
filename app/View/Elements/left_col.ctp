<div class="side_heading line-color"></div>
<div class="side_tile">
	<div class="news">
		<h4><span class="style4">ЗОЛОЧЕНИЕ ПО ИННОВАЦИОННОЙ ТЕХНОЛОГИИ:</span></h4>
		<br />
		<p class="contentll">Имитация золота – специальное покрытие для хромированных изделий</p> 
		<br />
		</div>
	</div>
	<br />
<div class="side_tile-gallery">
        <ul class="level2">
            <?php foreach($galleriesList as $gallery): ?>
                <div id='gallery-link'><li class='no-list'><a href="/<?=$gallery['Gallery']['url']?>/"><?=$gallery['Gallery']['type']?></a></li></div>
            <?php endforeach ?>
        </ul>
</div>
        <br />
<div class="side_heading line-color" id="padding"></div>
	<div class="side_tile">
	<div class="news">
		<p class="clear">
			<!--<a href="/wheels/" class="link_text">//-->Диски – обувь для Вашего авто:<!--</a>//-->
		</p>
		<br />
		<ul>
			<li><a href="/как-выбрать-диски/" class="link_text">Как выбирать диски</a>
			<br />
			<li><a href="/виды-колесных-дисков/" class="link_text">Виды автомобильных дисков</a>
			<br />
		    <li><a href="/уход-за-хромированными-дисками/" class="link_text">Уход и эксплуатация</a>
			<br />
            <li><a href="/repair/" class="link_text">Ремонт и покраска дисков</a>
			<br />        
			<li><a href="/вакуумное-хромирование/" class="link_text">Особенности вакуумного хромирования</a>
			<br />
        </ul>
		<br />
		<div class="center">
			<img src="/images/liberty.jpg" alt="Диски – обувь для Вашего авто" width="199" height="133" vspace="5" border="0" />
		</div>
		<br />
    </div>
	</div>

<div class="side_heading line-color" id="padding"></div>
	<div class="side_tile">
	<p class="clear">
		<a href="<?=Router::url(['controller' => 'pages', 'action' => 'constructor'])?>" class="link_text">Виртуальная примерка дисков</a>
		</p>
		<br />
		<div class="center">
			<a href="<?=Router::url(['controller' => 'pages', 'action' => 'constructor'])?>">
				<img src="/images/disc-measuring.jpg" alt="Виртуальная примерка дисков" width="135" height="90" vspace="5" border="0" title="Виртуальная примерка дисков" />
			</a>
		</div>
		<br />
</div>