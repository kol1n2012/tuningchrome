<div class="header-line line-color">
	<div class="logo imglogo">
		<a href="/" alt="ТЮНИНГ-ХРОМ (хромирование и золочение деталей)" title="10 ЛЕТ ВАКУУМНОГО ХРОМИРОВАНИЯ"><span class="right">Tuning</span><span class="left">Chrome</span></a>
	</div>
	<div class="icon-soc">
		<a class="instagram" href="https://www.instagram.com/chromzolotoi" target="_blank"></a>
		<a class="email" href="mailto:info@tuning-chrome.ru"></a>
	</div>
		<div class="phone"><span>8 (499) 390-39-29</span></div>
</div>
<div class="arrow" id="pointer"></div>
<div id="header-rectangle"><p class="address"><span>Пункт приема и оценки:</span><br />г.Москва<br />м.Текстильщики <br />Волжский бульвар, д.11</p></div>