<!-- Navigation menu-->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <!-- End of collapse block -->
        <a class="navbar-brand" href="/">Tuning-chrome</a>
        <div class="nav-collapse">
            <?php if($user = $this->Session->read('Auth.User')): ?>
                <ul class="nav navbar-nav">
                    <?php foreach($modules as $title => $sublinks): ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?=$title?><b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <?php foreach($sublinks as $sublink_title => $link): ?>
                                    <li><?=$this->Html->link($sublink_title, $link)?></li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                    <?php endforeach ?>
                </ul>
                <!-- Profile block -->
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user"></i>
                            <?=$user['name']?><b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=Router::url(array('controller' => 'users', 'action' => 'logout'))?>"><i class="fa fa-sign-out"></i> Выйти </a></li>
                        </ul>
                </ul>
                </li>
            <?php endif ?>
            <!-- End of Profile block -->
        </div>
    </div>
</div>
<!-- Navigation menu ends -->
