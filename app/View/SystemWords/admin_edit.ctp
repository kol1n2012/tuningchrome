<h1>Системные слова</h1>
<ul class="breadcrumbs breadcrumb">
    <li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
    </li>
    <li>
        <a href="<?=Router::url(array('action' => 'index'))?>">Системные слова</a>
    </li>
    <li class="active">Изменение</li>
</ul>
<div class="well">
    <?=$this->Form->create()?>
    <?=$this->Form->input('id')?>
    <?=$this->Form->input('name', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('code', array('label' => 'Код: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('value', array('label' => 'Значение: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'textarea'))?>
    <div class="form-actions">
        <?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
    </div>
    <?=$this->Form->end()?>
</div>
