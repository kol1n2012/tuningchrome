<h1>Системные слова</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
	<li class="active"><a href="<?=Router::url(array('action' => 'index'))?>">Системные слова</a></li>
</ul>
<div class="well form-inline">
    <a class="btn btn-success" href="<?=Router::url(array('action' => 'add'))?>"><i class="fa fa-file"></i> Создать</a>
</div>
<div id="list">
    <table class="table">
    	<thead>
    		<tr>
    			<th>Название</th>
    			<th>Код</th>
    			<th>Значение</th>
    			<th></th>
    		</tr>
    		<tbody>
    			<?php if($system_words): ?>
    			<?php foreach($system_words as $item): $item = $item['SystemWord']; ?>
    			<tr>
    				<td><?=$item['name']?></td>
    				<td><?=$item['code']?></td>
                    <td><?=$item['value']?></td>
    				<td>
                        <div class="pull-right">
        					<a class="btn btn-success" href="<?=Router::url(array('action' => 'edit', $item['id']))?>">
        						<i class="fa fa-pencil"></i> Изменить
        					</a>
        					<a class="btn btn-danger delete" href="<?=Router::url(array('action' => 'delete', $item['id']))?>">
        						<i class="fa fa-remove"></i> Удалить
        					</a>
                        </div>
    				</td>
    			</tr>
    			<?php endforeach ?>
    			<?php endif ?>
    		</tbody>
    	</thead>
    </table>
</div>
