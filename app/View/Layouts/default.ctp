<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title><?php echo $this->fetch('title'); ?></title>
    <?= $this->Html->css([
	    'bootstrap3.min.css',
	    'font-awesome.min.css',
	    'admin.styles'
	]); ?>
  </head>
  <body>
    <?=$this->element('Nav/modules')?>
    <div class="container">
        <?php if($messages = $this->Session->read('Message')): ?>
        <div id="messages">
            <?php echo $this->Flash->render() ?>
        </div>
        <?php endif ?>
        <?=$this->fetch('content'); ?>
    </div>
    <script>
	var hostname = 'http://<?=$_SERVER['HTTP_HOST']?>';
	</script>
	<?= $this->Html->script([
	    'jquery.min.js',
	    '/tinymce/tinymce.min.js',
	    '/tinymce/jquery.tinymce.min.js',
	    'bootstrap.min.js',
	    'admin.scripts.js'
	]); ?>
  </body>
</html>

