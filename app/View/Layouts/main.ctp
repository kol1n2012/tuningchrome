﻿<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?=$this->fetch('description') ?>" />
        <meta name="keywords" content="<?=$this->fetch('keywords') ?>" />
        <meta name="yandex-verification" content="905d009620de5c31" />

		<meta name="revisit" content="7 days" />
        <meta name="revisit-after" content="7 days" />
        <title><?=$this->fetch('title') ?></title>

	
        <!--	[if lte IE 8]>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <![endif]-->
		<script defer src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
        <style type="text/css">
            @import url("/css/style.css?v2");
            .style3 {color: #999900}
            .style4 {color: #FF0000}
            .style6 {
                font-size: 20px;
                color: #FFFFFF;
            }
            .style8 {font-size: 16px}

        </style>
		<!--[if IE]><link rel="stylesheet" href="/css/ie.css" type="text/css" /><![endif]-->

		
<script type="text/javascript">
  window.onload = function () {
	if(typeof String.prototype.trim!=='function'){
					String.prototype.trim=function(){
						return jQuery.trim(this);
							}
						}
};
</script>
<script defer src="/js/AC_RunActiveContent.js" type="text/javascript"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script defer src="https://www.googletagmanager.com/gtag/js?id=UA-119669389-1"></script>
    <script>
            window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-119669389-1');
    </script>
		<link rel="canonical" href="<?='https://'.$_SERVER['HTTP_HOST'].$this->fetch('canonical').'/' ?>">
    </head>
    <body>
	<div class="container">
    <div class="header-main">
                <?=$this->element('header') ?></div>
                <?=$this->element('Nav/menu') ?>
    <div class="admission"></div>


            <div class="main">
		    <div id="partners-title"><p class="partners-text">Нам доверяют постоянные клиенты</p></div>
			<div class="left_side">
                    <?=$this->element('left_col')?>
                </div>
                <div class="right_side">
					<div class="partners">
						<img src="/images/lotte-hotel-moscow.png" alt="" title="" class="partner-logo">
						<img src="/images/1492591216_cheviplus.png" alt="" title="" class="partner-logo">
						<img src="/images/kengurupro.png" alt="" title="" class="partner-logo">
						<img src="/images/Sportcarcenter-logo-web4s.png" alt="" title="" class="partner-logo">
					</div>
               <?php if($this->fetch('name')&&($this->fetch('name')!= 'Главная')): ?>
                    <div class="breadcrumbs">
						<a href="/"><img alt="На главную ТЮНИНГ-ХРОМ (хромирование деталей)" title="На главную ТЮНИНГ-ХРОМ (хромирование деталей, дисков, авто)" src="/images/home.gif"></a>
			            <? if($this->fetch('gallery_id')): ?>
							<span class="separator"></span>
							<a class="breadlink" href="/<?=$this->fetch('gallery_url')?>/"><?=$this->fetch('gallery_name')?></a>
						<? endif ?>
					<span class="separator"></span>
					<span><?=$this->fetch('name'); ?></span>
					</div>
				<? endif ?>
                    <?=$this->fetch('content')?>
                </div>
                <div class="remove_sides"></div>
            </div>
	
            <div class="footer line-color"><a href="/">&copy; 2009-<?=date('Y')?> "Тюнинг-Хром". Все права защищены. </a></div>
    </div>
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter48084485 = new Ya.Metrika({ id:48084485, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="//mc.yandex.ru/watch/48084485" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
            
    </body>
</html>