<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?=$this->fetch('description'); ?>" />
    <meta name="keywords" content="<?=$this->fetch('keywords'); ?>" />
    <meta name="revisit" content="7 days" />
    <meta name="revisit-after" content="7 days" />
    <title><?=$this->fetch('title'); ?></title>
        <!--	[if lte IE 8]>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <![endif]-->
		<script defer src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        @import url("/css/style.css?v2");
        .style3 {color: #999900}
        .style4 {color: #FF0000}
        .style6 {
            font-size: 20px;
            color: #FFFFFF;
        }
        .style8 {font-size: 16px}
    </style>
<script type="text/javascript">
  window.onload = function () {
	if(typeof String.prototype.trim!=='function'){
					String.prototype.trim=function(){
						return jQuery.trim(this);
							}
						}
};
</script>
<script defer src="/js/AC_RunActiveContent.js" type="text/javascript"></script>
		<? if($_SERVER['REQUEST_URI']=='/disc-measuring'): ?>
			<link rel="canonical" href="<?='https://'.$_SERVER['HTTP_HOST'].$this->fetch('canonical').'/disc-measuring' ?>">
			<? else: ?>
			<link rel="canonical" href="<?='https://'.$_SERVER['HTTP_HOST'].$this->fetch('canonical').'/' ?>">
		<? endif ?>
</head>
<body>
	<div class="container">
    <div class="header">
        <?=$this->element('header')?> </div>
        <?=$this->element('Nav/menu')?>
    <div class="admission"></div>
    <div class="main">
		<div class="breadcrumbs">
            <a href="/"><img alt="На главную ТЮНИНГ-ХРОМ (золочение и хромирование деталей)" title="На главную ТЮНИНГ-ХРОМ (хромирование авто и мото деталей, золочение)" src="/images/home.gif"></a><span class="separator"></span>
            <?php if($this->fetch('gallery_id')): ?>
            <a href="/gallery?id=<?=$this->fetch('gallery_id'); ?>&type=<?=$this->fetch('gallery_name'); ?>"><?=$this->fetch('gallery_name'); ?></a>
            <?php endif ?>
            <span><?=$this->fetch('name'); ?></span>
        </div>
        <?php if($this->fetch('name')): ?>
        <?php endif ?>
			<div id="partners-title"><p class="partners-text">Нам доверяют постоянные клиенты</p></div>
			<div class="partners">
						<img src="/images/lotte-hotel-moscow.png" alt="" title="" class="partner-logo">
						<img src="/images/1492591216_cheviplus.png" alt="" title="" class="partner-logo">
						<img src="/images/kengurupro.png" alt="" title="" class="partner-logo">
						<img src="/images/Sportcarcenter-logo-web4s.png" alt="" title="" class="partner-logo">
						<!--<img src="/images/imfabric-logo.png" alt="" title="" class="partner-logo">//-->
					</div>
        <?=$this->fetch('content')?>
    <div class="footer line-color"><a href="/">&copy; 2009-<?=date('Y')?> "Тюнинг-Хром". Все права защищены. </a></div>
        </div>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter48084485 = new Ya.Metrika({ id:48084485, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="//mc.yandex.ru/watch/48084485" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</div>
</body>
</html>