<h1>Robots.txt</h1>
<ul class="breadcrumbs breadcrumb">
    <li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
    </li>
    <li>
        <a href="<?=Router::url(array('action' => 'edit'))?>">Robots.txt</a>
    </li>
    <li class="active">Изменение</li>
</ul>
<div class="well">
    <?=$this->Form->create()?>
    <?=$this->Form->input('robots', array('label' => 'Контент файла: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'textarea', 'value' => $robots))?>
    <div class="form-actions">
        <?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
    </div>
    <?=$this->Form->end()?>
</div>
