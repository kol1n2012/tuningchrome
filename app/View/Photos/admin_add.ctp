<h1>Галлереи</h1>
<ul class="breadcrumbs breadcrumb">
    <li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
    </li>
    <li>
        <a href="<?=Router::url(array('controller' => 'galleries', 'action' => 'index'))?>">Галереи</a>
    </li>
	<li class="active">Добавление фото</li>
</ul>
<div class="well">
	<?=$this->Form->create(['type' => 'multipart/form-data'])?>
    <?=$this->Form->input('name', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
	<?=$this->Form->input('url', array('label' => 'Адрес страницы: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('description', array('label' => 'Описание:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('content', array('label' => 'Полное описание:', 'div' => 'form-group', 'class' => 'form-control edit', 'type' => 'textarea'))?>
    <?=$this->Form->input('weight', array('label' => 'Сортировка: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('alt', array('label' => 'Название картинки:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('small_pic', array('label' => 'Маленькая картинка: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'file'))?>
    <?=$this->Form->input('big_pic', array('label' => 'Большая картинка: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'file'))?>
    <?=$this->Form->input('published', ['type' => 'checkbox', 'checked' => true])?>
    <?=$this->Form->input('seo_title', array('label' => 'SEO title:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_keywords', array('label' => 'SEO keywords:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_description', array('label' => 'SEO description:', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('seo_h1', array('label' => 'SEO h1:', 'div' => 'form-group', 'class' => 'form-control'))?>
	<div class="form-actions">
		<?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
	</div>
	<?=$this->Form->end()?>
</div>