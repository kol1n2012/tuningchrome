<?php
$gallery = $photo['Gallery'];
$photo = $photo['Photo'];

$this->assign('seo_description', 'Наша компания занимается гальваническим хромированием никелированием дисков и деталей, ремонтом и покраской дисков автомобилей, мотоциклов, а также инкрустацией стразами Swarovski');
$this->assign('seo_keywords', 'тюнинг, хром, хромирование, никелирование, гальваника, хромирование дисков, никелирование дисков, хромирование деталей, никелирование деталей, покраска дисков, гальваническое хромирование, ремонт диска, хромирование авто, хромированные диски, тюнинг авто');
$this->assign('name', $photo['name']);
$this->assign('gallery_name', $gallery['type']);
$this->assign('gallery_url', $gallery['url']);
$this->assign('canonical', '/'.$gallery['url'].'/'.$photo['url']);
$this->assign('gallery_id', $gallery['id']);
$this->assign('title', $photo['seo_title'] ? $photo['seo_title'] : 'Тюнинг-Хром хромирование и никелирование дисков и деталей, ремонт покраска дисков, гальваника и вакуумное хромирование - Хромирование дисков - ' . $photo['name']);
$this->assign('description', $photo['seo_description']);
$this->assign('keywords', $photo['seo_keywords']);
$this->assign('seo_h1', $photo['seo_h1']);

?>
<div class="side_heading line-color">
    <span><?=$photo['name']?></span>
</div>

<div class="side_tile">
<?if(isset($_POST["prev_photo"]) || isset($_POST["next_photo"])):?>
<div class="photo-pagination">
	<?if(isset($_POST["prev_photo"])):?>
		<a class="button photo-pagination-button prev" href="/<?=$gallery['url']?>/<?=$_POST["prev_photo"]?>/">Предыдущее</a>
	<?endif?>
	<?if(isset($_POST["next_photo"])):?>
		<a class="button photo-pagination-button next" href="/<?=$gallery['url']?>/<?=$_POST["next_photo"]?>/">Следующее</a>
	<?endif?>
</div>
<?endif?>
    <div class="auto_item">
        <img src="/images/gallery/big/<?=$photo['big_pic']?>" class="auto_gallery_item" alt="<?=$photo['alt'] ? $photo['alt'] : $photo['name'] ?>" title="<?=$photo['name']?>">
        <br><span class="content"> <?=$photo['seo_h1'] ? '<h1>' . $photo['seo_h1'] . '</h1>' : ''?></span><br><br><span class="description"><?=$photo['content']?$photo['content']:$photo['description']?></span>
       
    </div>
</div>
<div class="side_bottom_right"></div>