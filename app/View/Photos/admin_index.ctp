<h1>Галерея "<?=$gallery['Gallery']['type']?>"</h1>
<ul class="breadcrumbs breadcrumb">
	<li><a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a></li>
    <li><a href="<?=Router::url(array('controller' => 'galleries', 'action' => 'index'))?>">Галереи</a></li>
	<li class="active"><a href="<?=Router::url(array('action' => 'index'))?>">Фотографии</a></li>
</ul>
<div class="well form-inline">
    <a class="btn btn-success" href="<?=Router::url(array('action' => 'add', $gallery['Gallery']['id']))?>"><i class="fa fa-file"></i> Создать</a>
</div>
<div id="list">
    <table class="table">
    	<thead>
    		<tr>
    			<th>Название</th>
				<th>Адрес страницы</th>
    			<th>Сортировка</th>
                <th>Название фото</th>
                <th>Описание</th>
                <th>Маленькое фото</th>
                <th>Большое фото</th>
    			<th></th>
    		</tr>
    		<tbody>
    			<?php if($gallery['Photo']): ?>
    			<?php foreach($gallery['Photo'] as $item): ?>
    			<tr>
    				<td><?=$item['name']?></td>
					<td><?=$item['url']?></td>
                    <td><?=$item['weight']?></td>
                    <td><?=$item['alt']?></td>
                    <td><?=$item['description']?></td>
                    <td><img width="150" class="thumbnail" src="/images/gallery/<?=$item['small_pic']?>"></td>
                    <td><img width="150" class="thumbnail" src="/images/gallery/big/<?=$item['big_pic']?>"></td>
    				<td>
                        <div class="pull-right">
        					<a class="btn btn-success" href="<?=Router::url(array('action' => 'edit', $item['id']))?>">
        						<i class="fa fa-pencil"></i> Изменить
        					</a>
        					<!--<a class="btn btn-danger delete" href="<?=Router::url(array('action' => 'delete', $gallery['Gallery']['id'], $item['id']))?>">
        						<i class="fa fa-remove"></i> Удалить
        					</a>-->
                        </div>
    				</td>
    			</tr>
    			<?php endforeach ?>
    			<?php endif ?>
    		</tbody>
    	</thead>
    </table>
</div>