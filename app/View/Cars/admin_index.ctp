<h1>Модели авто</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
	<li class="active"><a href="<?=Router::url(array('action' => 'index'))?>">Модели авто</a></li>
</ul>
<div class="well form-inline">
    <a class="btn btn-success" href="<?=Router::url(array('action' => 'add'))?>"><i class="fa fa-file"></i> Создать</a>
</div>
<div id="list">

    <ul class="pagination">
        <?php $pages = ceil($count/20); ?>
        <?php $currentPage = isset($this->request->query['page']) ? $this->request->query['page'] : 1 ?>
        <?php for($page = 1; $page <= $pages; $page++): ?>
            <?php if($currentPage == $page): ?>
                <li class="active"><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php else: ?>
                <li><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php endif ?>
        <?php endfor ?>
    </ul>
    <table class="table">
    	<thead>
    		<tr>
    			<th>Марка</th>
                <th>Модель</th>
                <th>Маленькое фото</th>
                <th>Большое фото</th>
    			<th></th>
    		</tr>
    		<tbody>
    			<?php if($cars): ?>
    			<?php foreach($cars as $item):  $carType = $item['CarType']; $item = $item['Car'];?>
    			<tr>
    				<td><a href="<?=Router::url(['controller' => 'car_types', 'action' => 'edit', $carType['id']])?>"><?=$carType['type']?></a></td>
                    <td><?=$item['description']?></td>
                    <td><img width="150" class="thumbnail" src="/flash/<?=$carType['id']?>/<?=$item['small_pic']?>"></td>
                    <td><img width="150" class="thumbnail" src="/flash/<?=$carType['id']?>/<?=$item['big_pic']?>"></td>
    				<td>
                        <div class="pull-right">
        					<a class="btn btn-success" href="<?=Router::url(array('action' => 'edit', $item['id']))?>">
        						<i class="fa fa-pencil"></i> Изменить
        					</a>
        					<a class="btn btn-danger delete" href="<?=Router::url(array('action' => 'delete', $item['id']))?>">
        						<i class="fa fa-remove"></i> Удалить
        					</a>
                        </div>
    				</td>
    			</tr>
    			<?php endforeach ?>
    			<?php endif ?>
    		</tbody>
    	</thead>
    </table>
    <ul class="pagination">
        <?php $pages = ceil($count/20); ?>
        <?php $currentPage = isset($this->request->query['page']) ? $this->request->query['page'] : 1 ?>
        <?php for($page = 1; $page <= $pages; $page++): ?>
            <?php if($currentPage == $page): ?>
                <li class="active"><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php else: ?>
                <li><a href="?page=<?=$page?>"><?=$page?></a></li>
            <?php endif ?>
        <?php endfor ?>
    </ul>
</div>
