<h1>Модели авто</h1>
<ul class="breadcrumbs breadcrumb">
	<li>
		<a href="<?=Router::url(array('controller' => 'site', 'action' => 'index'))?>">Главная</a>
	</li>
    <li>
		<a href="<?=Router::url(array('action' => 'index'))?>">Модели авто</a>
	</li>
	<li class="active">Добавление</li>
</ul>
<div class="well">
	<?=$this->Form->create(['type' => 'multipart/form-data'])?>
    <?=$this->Form->input('car_type_id', array('label' => 'Марка авто: *', 'div' => 'form-group', 'class' => 'form-control', 'options' => $car_types))?>
	<?=$this->Form->input('description', array('label' => 'Название: *', 'div' => 'form-group', 'class' => 'form-control'))?>
    <?=$this->Form->input('small_pic', array('label' => 'Маленькая картинка: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'file'))?>
    <?=$this->Form->input('big_pic', array('label' => 'Большая картинка: *', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'file'))?>
	<div class="form-actions">
		<?=$this->Form->submit('Сохранить', array('class' => 'btn btn-success'))?>
	</div>
	<?=$this->Form->end()?>
</div>
