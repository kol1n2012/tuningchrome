<?php
$this->assign('title', $gallery['Gallery']['seo_title'] ? $gallery['Gallery']['seo_title'] : $gallery['Gallery']['type']);
$this->assign('description', $gallery['Gallery']['seo_description']);
$this->assign('keywords', $gallery['Gallery']['seo_keywords']);
$this->assign('name', $gallery['Gallery']['type']);
$this->assign('canonical', '/'.$gallery['Gallery']['url']);
?>
<div class="left_side">
	<?=$this->element('left_col')?>
</div>
<div class="right_side">
	<div class="side_heading line-color">
		<span><?=$gallery['Gallery']['type']?></span>
	</div>
<div class="side_tile">
	<!--<form >
			<table cellpadding="2" cellspacing="0" align="center">
				<tbody>
					<tr class="KT_row_filter">
						<td><input type="text" name="tfi_listgallery1_name" id="tfi_listgallery1_name" value="" size="20" maxlength="100" kl_vkbd_parsed="true"> <input type="submit" name="tfi_listgallery1" value="&#65279;Поиск" kl_vkbd_parsed="true"></td>
					</tr>
				</tbody>
			</table>
	</form>-->
		<?php foreach($photos as $photo): $photo = $photo['Photo']?>
		<div class="auto-gallery">
			<div class="auto">
				<a href="/<?=$gallery['Gallery']['url']?>/<?=$photo['url']?>/"><img src="/images/gallery/<?=$photo['small_pic']?>" alt="<?=$photo['name']?>" title="<?=$photo['name']?>"></a>
				<a href="/<?=$gallery['Gallery']['url']?>/<?=$photo['url']?>/" class="link_text"><?=$photo['name']?></a><span class="description"><?=$photo['description']?></span><br>
				<a href="/<?=$gallery['Gallery']['url']?>/<?=$photo['url']?>/" class="link_button">подробнее</a>
			</div>
		</div>
		<?php endforeach ?>
		<div class="remove_sides"></div>
			<?=$this->element('front_pagination')?>
		<div class="gallery-content">
			<?=$gallery['Gallery']['content']?>
		</div>
		<div class="remove_sides"></div>
</div>
</div>
		<div class="remove_sides"></div>