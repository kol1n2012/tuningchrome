<div class="rows">
    <h1>Панель управления</h1>
    <div class="well">
        Вы находитесь в системе управления контентом. Воспользуйтесь меню, расположенным ниже для того чтобы попасть в нужный вам раздел редактирования.
    </div>
    <?php foreach($modules as $title => $sublinks): ?>
    <div class="col-md-3">
    <h2><?=$title?></h2>
    <ul class="list-group">
        <?php foreach($sublinks as $sublink_title => $link): ?>
            <li class="list-group-item"><?=$this->Html->link($sublink_title, $link)?></li>
        <?php endforeach ?>
    </ul>
    </div>
    <?php endforeach ?>
</div>