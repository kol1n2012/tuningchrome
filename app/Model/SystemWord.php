<?php

class SystemWord extends AppModel {

    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        ),
        'code' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Системное слово с таким кодом уже существует!'
            )
        ),
        'value' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        ),
    );


}