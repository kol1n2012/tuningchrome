<?php

class Car extends AppModel {

    public $belongsTo = ['CarType'];

    public $validate = array(
        'description' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );

    public function beforeSave($options = []){
        parent::beforeSave();
        if(!$this->data[$this->alias]['small_pic']['tmp_name'] and !$this->data[$this->alias]['small_pic']['tmp_name']){
            unset($this->data[$this->alias]['small_pic']);
        }
        if(!$this->data[$this->alias]['big_pic']['tmp_name'] and !$this->data[$this->alias]['big_pic']['tmp_name']){
            unset($this->data[$this->alias]['big_pic']);
        }

        if(
            !empty($this->data[$this->alias]['small_pic']) and
            !empty($this->data[$this->alias]['car_type_id']) and
            is_array($this->data[$this->alias]['small_pic']) and
            $this->data[$this->alias]['small_pic']['tmp_name']
        ){
            if(!file_exists(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id'])){
                mkdir(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id']);
            }
            copy($this->data[$this->alias]['small_pic']['tmp_name'], WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id'] . DS . $this->data[$this->alias]['small_pic']['name']);
            $this->data[$this->alias]['small_pic'] = $this->data[$this->alias]['small_pic']['name'];
        }
        if(
            !empty($this->data[$this->alias]['big_pic']) and
            !empty($this->data[$this->alias]['car_type_id']) and
            is_array($this->data[$this->alias]['big_pic']) and
            $this->data[$this->alias]['big_pic']['tmp_name']
        ){
            if(!file_exists(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id'])){
                mkdir(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id']);
            }
            copy($this->data[$this->alias]['big_pic']['tmp_name'], WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['car_type_id'] . DS . $this->data[$this->alias]['big_pic']['name']);
            $this->data[$this->alias]['big_pic'] = $this->data[$this->alias]['big_pic']['name'];
        }
        return true;
    }


}