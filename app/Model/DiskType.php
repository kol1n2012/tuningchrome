<?php

class DiskType extends AppModel {

    public $hasMany = ['Disk'];

    public $validate = array(
        'type' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );


}