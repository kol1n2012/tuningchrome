<?php
/**
 * Uploadable Behavior class
 *
 * Позволяет загружать файлы на сервер.
 *
 * public $actsAs = array(
 *      'Uploadable' => array(
 *          'field' => array (
 *          	'destination' => "files/models", // По умолчанию путь /files/модельвмножественномчисле
 *          	'validate'    => false,          // по умолчанию нет или должно быть указано название функции в модели
 *				'duplicates'  => 'rename'        // rename - переименовывать, true - перезаписывать, false -  запрещено
 *          )
 *      )
 * )
 *
 *
 *
 * @copyright   Decafe Studio, 2008, Kirill "Hellbot" Losev
 * @license	    http://www.opensource.org/licenses/mit-license.php The MIT License
 *
 */

class UploadableBehavior extends ModelBehavior {

    /**
     * Настройки для каждой модели
     *
     * @public array
     * @access private
     * @see Model::$alias
     */
    public $settings = array();

    /**
     * Параметры выполнения
     *
     * @public array
     */
    public $runtime = array();

    /**
     * Настраиваем behavior для работы с моделью.
     *
     * @param AppModel $model Модель
     * @param array $config Конфигурация для Модели
     * @access public
     */
	public function setup(Model $model, $config = array()) {
        $defaults = array(
            'destination'   => WWW_ROOT . 'files' . DS . strtolower(Inflector::pluralize($model->name)) . DS,
            'validate'      => false,
            'duplicates'    => 'rename'
        );

        if (!is_array($config)) {
            $config = array($config);
        }

        foreach ($config as $field => $fconfig) {
            if (!is_array($fconfig)) {
                if ($model->hasField($fconfig)) { // Перечисление полей
                    $field = $fconfig;
                    $fconfig = array();
                } elseif ($model->hasField($field)) { // поле => путь
                    $fconfig = array('destination' => $fconfig);
                } else {
                    // фиг знает чо
                    continue;
                }
            }
            $this->settings[$model->alias][$field] = am($defaults, $fconfig);
        }
        return true;
	}
    
    /**
     * Creates a directory
     *
     * @param Model $model Model instance
     * @param string $field Name of field being modified
     * @param string $destDir directory to create
     * @return bool
     */
	protected function mkPath($destDir, $permission) {
		if (!file_exists($destDir)) {
			mkdir($destDir, $permission, true);
			chmod($destDir, $permission);
		}
		return true;
	}
    
    protected function getPath(Model $model, $path) {
        return str_replace(
            array(
                '{WWW_ROOT}',
                '{DS}',
                '{model}',
                '{foreignKey}'
            ),
            array(
                WWW_ROOT,
                DS,
                Inflector::underscore($model->alias),
                !empty($model->data[$model->alias]['foreign_key']) ? 
                    $model->data[$model->alias]['foreign_key'] : '{foreignKey}'
            ),
            $path
        );
    }

    /**
     * Очищаем настройки модели. Выполнятся когда динамически отсоединяем behavior от модели
     *
     * @param AppModel $model Модель используящая behavior
     * @access public
     * @see BehaviorCollection::detach()
     */
	function cleanup(Model $model) {
        parent::cleanup($model);
	}
    
    /**
    * Updates a database record with the necessary extra data
    *
    * @param Model $model Model instance
    * @param array $data array containing data to be saved to the record
    * @return void
    */
	protected function updateRecord(Model $model, $data) {
		if (!empty($data[$model->alias])) {
			return $model->updateAll($data[$model->alias], array(
				$model->alias . '.' . $model->primaryKey => $model->id
			));
		}
	}

    /**
     * Проверка входящих данных на их правильность.
     *
     * @param AppModel $model Модель используящая behavior
     * @return boolean Возращает true если можно продолжать, false если нет
     * @access public
     */
	public function beforeValidate(Model $model, $options = array()) {
        if (!isset($this->settings[$model->alias]) or empty($this->settings[$model->alias])) {
            return true; // Нечего проверять для этой модели (и вообще не ясно как мы сюда попали)
        }
        foreach ($this->settings[$model->alias] as $field => $config) {
            if (!isset($model->data[$model->alias][$field])) {
                continue; // Нет интересующего поля
            }

            if (empty($model->data[$model->alias][$field])) { // Пустая строка ?!
                unset($model->data[$model->alias][$field]); // Не сохраняем ее
                continue;
            }
            if (!is_array($model->data[$model->alias][$field]) || 
                empty($model->data[$model->alias][$field]['tmp_name']) ||
                empty($model->data[$model->alias][$field]['error'])) {
                continue;
            }

            if (!is_array($model->data[$model->alias][$field])) {
                continue; // Хм, не наш клиент (тут просто строка, видимо руками поправили)
            }

            $upload = &$model->data[$model->alias][$field];
            if ($upload['error'] != UPLOAD_ERR_OK) {
                if ($upload['error'] != UPLOAD_ERR_NO_FILE) {
                    $model->invalidate($field, $this->msgErrorUpload($upload['error']));
                }
                unset($model->data[$model->alias][$field]); // Ничего не сохраняем
                continue;
            }
            if (!is_uploaded_file($upload['tmp_name'])) { // Меня пугает когда такое происходит !
                // Пугаемся
                $this->log('Upload file error, possible hacking attempt', LOG_WARNING);

                // И ничего не сохраняем
                $model->invalidate($field, UPLOAD_ERR_NO_FILE);
                unset($model->data[$model->alias][$field]);

                continue;
            }
            if (!$config['duplicates']) {
                if (file_exists($config['destination'] . $upload['name'])) {
                    $model->invalidate($field, __('Файл с таким именем уже существует', true));
                    unset($model->data[$model->alias][$field]);
                    continue;
                }
            }


            if ($config['validate']) {
                if (method_exists($model,$config['validate'])) {
                    $model->$config['validate']($field, $upload);
                }
            }
        }
        return empty($model->validationErrors);
	}

    /**
     * Действия перед сохранением
     *
     * @param AppModel $model Модель используящая behavior
     * @return boolean Возращает true если можно продолжать, false если нет
     * @access public
     */
	public function beforeSave(Model $model, $options = array()) {
        if (!isset($this->settings[$model->alias]) or empty($this->settings[$model->alias])) {
            return true;
        }

        $savedModel = array();
        foreach ($this->settings[$model->alias] as $field => $config) {
            if (!isset($model->data[$model->alias][$field])) {
                continue;
            }
            
            if (
                is_array($model->data[$model->alias][$field]) && 
                (
                    empty($model->data[$model->alias][$field]['tmp_name']) || 
                    !isset($model->data[$model->alias][$field]['error']) || 
                    $model->data[$model->alias][$field]['error'] === null
                )
                ) {
                unset($model->data[$model->alias][$field]);
                continue;
            }
            
            if (!is_array($model->data[$model->alias][$field]) || 
                empty($model->data[$model->alias][$field]['tmp_name']) ||
                !isset($model->data[$model->alias][$field]['error']) || 
                $model->data[$model->alias][$field]['error'] === null) {
                continue;
            }
            
            $upload = &$model->data[$model->alias][$field];

            if ($upload['error'] != UPLOAD_ERR_OK) {
                unset($model->data[$model->alias][$field]);
                continue;
            }
            
            $model->data[$model->alias][$field . '_mime'] = $upload['type'];
            $model->data[$model->alias][$field . '_size'] = $upload['size'];
            $model->data[$model->alias][$field . '_name'] = $upload['name'];

            $filename = str_replace(' ', '_', $upload['name']);
            
            // Заменим все плейсхолдеры в поле destination
            $config['destination'] = $this->getPath($model, $config['destination']);
            // Подставим директорию, если поле задано в настройках и в модели есть соответствующее поле
            if (!empty($config['dir']) && $model->hasField('dir')) {
                $model->data[$model->alias]['dir'] = $this->getPath($model, $config['dir']);
            }
            
            // Создаем новую папку при необходимости
            $this->mkPath($config['destination'], 0777);

            if ($config['duplicates']) {
                if (file_exists($config['destination'] . $filename) and ($config['duplicates'] == 'rename')) {
                    // Переименовываем файлик
                    $pathinfo = pathinfo($filename);
                    if (isset($pathinfo['extension']) ) {
                        // Извлекаем расширение
                        $pathinfo['basename'] = basename($pathinfo['basename'], '.' . $pathinfo['extension']);
                    }

                    do {
                        $filename = $pathinfo['basename'] . '.' . md5(rand()) . '.' . $pathinfo['extension'];
                    } while (file_exists($config['destination'] . $filename));

                } elseif (is_string($config['duplicates']) and method_exists($model, $config['duplicates'])) {
                    $filename = $model->$config['duplicates']($field, $filename);
                };
            }
            if (!$result = move_uploaded_file($upload['tmp_name'], $config['destination'] . $filename)) {
                if (count(scandir($config['destination'])) === 2) {
                    rmdir($config['destination']);
                }
                $model->invalidate($field, __('Невозможно перенести файл, обратитесь к системному администратору',true));
                unset($model->data[$model->alias][$field]);
                continue;
            }

            // Меняем права доступа (rw-rw-r)
            @chmod($config['destination'] . $filename, 0664);
            if ($model->exists()) {
                if (empty($savedModel)) {
                    $savedModel = $model->find('first', array(
                        'conditions' => array($model->name . '.' . $model->primaryKey => $model->id),
                        'fields'     => array_keys($this->settings[$model->name]),
                        'recursive'  => -1));
                }
//                if (!empty($savedModel[$model->alias][$field]) and file_exists($config['destination'] . $savedModel[$model->alias][$field])) {
//                    @unlink($config['destination'] . $savedModel[$model->alias][$field]);
//                }
            }
            
            unset($model->data[$model->alias][$field]);
            $model->data[$model->alias][$field] = $filename;
            
            // При связи hasMany
            if (!empty($model->data[$model->alias]['foreign_key'])) {
                $savedModel = $model->find('first', array(
                    'conditions' => array(
                        $model->name . '.' . $field => $filename,
                    $model->name . '.foreign_key' => $model->data[$model->alias]['foreign_key']
                    ),
                    'fields'     => array_merge(array($model->primaryKey), array_keys($this->settings[$model->name])),
                    'recursive'  => -1
                ));
                // Проверим существует ли данная запись и файл
                if (!empty($savedModel[$model->alias][$field]) and file_exists($config['destination'] . $savedModel[$model->alias][$field])) {
                    // при попытке установить первичный ключ и сохранить, cake делает insert вместо update
                    // вернем первичный ключ и будем смотреть по нему
                    $model->{$model->primaryKey} = $savedModel[$model->alias][$model->primaryKey];
                    return false;
                }
            }
        }
        return empty($model->validationErrors);
	}

    /**
     * Действия перед удаление модели
     *
     * @param AppModel $model Модель используящая behavior
     * @param boolean $cascade Если true, удаляются так же модели связаные с этой
     * @return boolean Возращает true если можно продолжать, false если нет
     * @access public
     */

	public function beforeDelete(Model $model, $cascade = true) {
        /**
         * Умная функия удаления, если у нас есть информация о полях в data модели,
         * будут использованы данные от туда.
         *
         */
	    $deleteQueue = $queryFields = array();
        
        // Дополнительное поле, которое необходимо вытащить
        if ($model->hasField('foreign_key')) {
            $queryFields[] = 'foreign_key';
            $queryFields[] = $model->primaryKey;
        }
	    foreach ($this->settings[$model->alias] as $k => $v) {
            // Не используем неактуальные данные, т.к. это не подходит к концепции hasMany
//	        if (isset($model->data[$model->alias][$k])) {
//	            $deleteQueue[$k] = $model->data[$model->alias][$k];
//	        } else {
	            $queryFields[] = $k;
//	        }
	    }
	    // Запрашиваем только те поля которые не нашли
	    if (!empty($queryFields)) {
            // $model->read() - заменяет $model->data, а там могли быть нужные для других данные
	        $data = $model->find('first', array(
	           'conditions' => array(
	               $model->primaryKey => $model->id
	           ),
	           'fields'     => $queryFields,
	           'recursive'  => -1
	        ));
	        if ($data) {
	            if (!isset($model->data[$model->alias])) {
	                $model->data[$model->alias] = array();
	            }
	            $model->data[$model->alias] = am($model->data[$model->alias], $data[$model->alias]);
	            $deleteQueue = am($deleteQueue, $data[$model->alias]);
	        };
	    };

	    if (!empty($deleteQueue)) {
	        $this->runtime[$model->alias]['deleteQueue'] = $deleteQueue;
	    }
        
        foreach($this->settings[$model->alias] as $field => $config) {
            if (isset($this->settings[$model->alias][$field]['destination'])) {
                $this->settings[$model->alias][$field]['destination'] = $this->getPath($model, $this->settings[$model->alias][$field]['destination']);                
            }
        }
	    return true;
	}

    /**
     * Действия после удаления модели
     *
     * @param AppModel $model Модель используящая behavior
     * @access public
     */
	public function afterDelete(Model $model) {
	    if (isset($this->runtime[$model->alias]) and isset($this->runtime[$model->alias]['deleteQueue'])) {
	        foreach ($this->settings[$model->alias] as $field => $config) {
                $filename = $config['destination'] . $this->runtime[$model->alias]['deleteQueue'][$field];
                if (file_exists($filename)) {
                    @unlink($filename);
                }
                if (is_dir($config['destination']) && count(scandir($config['destination'])) === 2) {
                    rmdir($config['destination']);
                }
            }
            unset($this->runtime[$model->alias]);
	    }
	    return true;
	}


	/**
	 * Возвращает текстовое описание ошибки при загрузке файла по ее коде
	 *
	 * @param interger $errorCode Код ошибки
	 * @return string
	 * @access public
	 */
    public function msgErrorUpload($errorCode) {
        $errorCodeValues = array(
            UPLOAD_ERR_OK         => false,
            UPLOAD_ERR_INI_SIZE   => 'Размер файла не должен превышать размера указаного в настройках сервера',
            UPLOAD_ERR_FORM_SIZE  => 'Размер файла превысил допустимые нормы',
            UPLOAD_ERR_PARTIAL    => 'Файл получен частично, попробуйте еще раз',
            UPLOAD_ERR_NO_FILE    => 'Файл не был отправлен',
            UPLOAD_ERR_NO_TMP_DIR => 'Не указана временная директория для файлов на сервере',
            UPLOAD_ERR_CANT_WRITE => 'Сервер не может сохранить файл на диск'
        );

        if ( isset($errorCodeValues[$errorCode]) ) {
            return __($errorCodeValues[$errorCode], true);
        }

        return false;
    }
}

?>