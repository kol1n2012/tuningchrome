<?php

class Disk extends AppModel {

    public $belongsTo = ['DiskType'];

    public $validate = array(
        'description' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );

    public function beforeSave($options = []){
        parent::beforeSave();
        if(!$this->data[$this->alias]['pic']['tmp_name'] and !$this->data[$this->alias]['pic']['tmp_name']){
            unset($this->data[$this->alias]['pic']);
        }

        if(
            !empty($this->data[$this->alias]['pic']) and
            !empty($this->data[$this->alias]['disk_type_id']) and
            is_array($this->data[$this->alias]['pic']) and
            $this->data[$this->alias]['pic']['tmp_name']
        ){
            if(!file_exists(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['disk_type_id'])){
                mkdir(WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['disk_type_id']);
            }
            copy($this->data[$this->alias]['pic']['tmp_name'], WWW_ROOT . 'flash' . DS . $this->data[$this->alias]['disk_type_id'] . DS . $this->data[$this->alias]['pic']['name']);
            $this->data[$this->alias]['pic'] = $this->data[$this->alias]['pic']['name'];
        }
        return true;
    }


}