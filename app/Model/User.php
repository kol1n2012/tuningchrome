<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public $validate = array(
        'email' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Пользователь с таким email уже существует!'
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        ),
        'name' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        ),
    );

    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}