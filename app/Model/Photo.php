<?php

class Photo extends AppModel {

    public $belongsTo = ['Gallery'];

    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        ),
        'description' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );

    public function beforeSave($options = []){
        parent::beforeSave();
        if(!$this->data[$this->alias]['small_pic']['tmp_name'] and !$this->data[$this->alias]['small_pic']['tmp_name']){
            unset($this->data[$this->alias]['small_pic']);
        }
        if(!$this->data[$this->alias]['big_pic']['tmp_name'] and !$this->data[$this->alias]['big_pic']['tmp_name']){
            unset($this->data[$this->alias]['big_pic']);
        }

        if(
            !empty($this->data[$this->alias]['small_pic']) and
            is_array($this->data[$this->alias]['small_pic']) and
            $this->data[$this->alias]['small_pic']['tmp_name']
        ){
            copy($this->data[$this->alias]['small_pic']['tmp_name'], WWW_ROOT . 'images' . DS . 'gallery' . DS . $this->data[$this->alias]['small_pic']['name']);
            $this->data[$this->alias]['small_pic'] = $this->data[$this->alias]['small_pic']['name'];
        }
        if(
            !empty($this->data[$this->alias]['big_pic']) and
            is_array($this->data[$this->alias]['big_pic']) and
            $this->data[$this->alias]['big_pic']['tmp_name']
        ){
            copy($this->data[$this->alias]['big_pic']['tmp_name'], WWW_ROOT . 'images' . DS . 'gallery' . DS . 'big' . DS . $this->data[$this->alias]['big_pic']['name']);
            $this->data[$this->alias]['big_pic'] = $this->data[$this->alias]['big_pic']['name'];
        }
        return true;
    }


}