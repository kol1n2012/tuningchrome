<?php

class CarType extends AppModel {

    public $hasMany = ['Car'];

    public $validate = array(
        'type' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );


}