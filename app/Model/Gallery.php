<?php

class Gallery extends AppModel {

    public $hasMany = ['Photo' => [
	    'order' => 'Photo.weight'
    ]];

    public $validate = array(
        'type' => array(
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Это поле должно быть заполнено!'
            )
        )
    );


}