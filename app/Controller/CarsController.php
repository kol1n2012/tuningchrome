<?php

class CarsController extends AppController {

    public $name = 'Cars';

    public $uses = ['Car', 'CarType'];

    public function admin_index() {
        $this->set([
            'cars' => $this->Car->find('all', [
                'limit' => 20,
                'page' => isset($this->request->query['page']) ? $this->request->query['page'] : 1
            ]),
            'count' => $this->Car->find('count')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->Car->save($this->request->data)) {
                $this->Flash->success('Модель машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
        $this->set([
            'car_types' => $this->CarType->find('list', [
                'fields' => ['id', 'type']
            ])
        ]);
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->Car->save($this->request->data)) {
                $this->Flash->success('Модель машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->Car->findById($id);
        }
        $this->set([
            'car_types' => $this->CarType->find('list', [
                'fields' => ['id', 'type']
            ])
        ]);
    }

    public function admin_delete($id = null) {
        if ($this->Car->delete($id)) {
            $this->Flash->success('Модель машины удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
