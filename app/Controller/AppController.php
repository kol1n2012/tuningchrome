<?php
	
App::uses('Controller', 'Controller');
App::uses('RcAuthenticate', 'Controller/Component/Auth');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email'),
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'sha256'
                    )
                )
            )
        ),
        'Session',
        'Flash'
    );
    
    public $modules = array(
            'Контент' => array(
                'Страницы'   => array('controller' => 'pages', 'action' => 'index'),
                'Галереи'    => array('controller' => 'galleries', 'action' => 'index')
            ),
            'Конструктор' => array(
                'Марки авто'   => array('controller' => 'car_types', 'action' => 'index'),
                'Модели авто'  => array('controller' => 'cars', 'action' => 'index'),
                'Марки дисков' => array('controller' => 'disk_types', 'action' => 'index'),
                'Диски'        => array('controller' => 'disks', 'action' => 'index')
            ),

            'Настройки' => array(
                'Системные слова'   => array('controller' => 'system_words', 'action' => 'index'),
                'Пользователи'      => array('controller' => 'users', 'action' => 'index'),
                'Robots.txt'        => array('controller' => 'robots', 'action' => 'edit')
            )
        );

    public function beforeFilter()
    {
        if($this->params->prefix != 'admin'){
            $this->Auth->allow();
        }
        parent::beforeFilter();
    }

    public function beforeRender()
    {
        App::import('Model', 'Gallery');
        $Gallery = new Gallery();
        $galleriesList = $Gallery->find('all', array(
	        'order' => array(
		        'Gallery.weight'
	        )
        ));
        $this->set('modules', $this->modules);
        $this->set('galleriesList', $galleriesList);
    }

}
