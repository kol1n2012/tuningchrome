<?php
class PhotosController extends AppController {
    public $name = 'Photos';
    public $uses = ['Photo', 'Gallery'];
    public function admin_index($galleryId) {
        $this->set([
            'gallery' => $this->Gallery->findById($galleryId)
        ]);
    }

    public function admin_add($galleryId) {
        if (!empty($this->request->data)) {
            $data = $this->request->data;
            $data['Photo']['gallery_id'] = $galleryId;
            if ($this->Photo->save($data)) {
                $this->Flash->success('Фотография сохранена');
                return $this->redirect(array('action' => 'index', $data['Photo']['gallery_id']));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->Photo->save($this->request->data)) {
                $this->Flash->success('Фотография сохранена');
                return $this->redirect(array('action' => 'index', $this->request->data['Photo']['gallery_id']));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->Photo->findById($id);
        }
    }

    public function admin_delete($galleryId, $id = null) {
        if ($this->Photo->delete($id)) {
            $this->Flash->success('Фотография удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index', $galleryId));
    }

    public function show($id) {
        $this->layout = 'main';
        $this->set([
            'photo' => $this->Photo->findById($id)
        ]);
    }
}