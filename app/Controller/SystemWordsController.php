<?php

class SystemWordsController extends AppController {

    public $name = 'SystemWords';

    public function admin_index() {
        $this->set([
            'system_words' => $this->SystemWord->find('all')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->SystemWord->save($this->request->data)) {
                $this->Flash->success('Системное слово сохранено');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->SystemWord->save($this->request->data)) {
                $this->Flash->success('Системное слово сохранено');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->SystemWord->findById($id);
        }
    }

    public function admin_delete($id = null) {
        if ($this->SystemWord->delete($id)) {
            $this->Flash->success('Системное слово удалено');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
