<?php

class RobotsController extends AppController {

    public $uses = false;

    public function admin_edit() {

        if($this->request->data and !empty($this->request->data['robots'])){
            file_put_contents(WWW_ROOT. 'robots.txt', $this->request->data['robots']);
        }
        $this->set([
            'robots' => file_get_contents(WWW_ROOT . 'robots.txt')
        ]);
    }
    
}