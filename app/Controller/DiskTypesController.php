<?php

class DiskTypesController extends AppController {

    public $name = 'DiskTypes';

    public function admin_index() {
        $this->set([
            'disk_types' => $this->DiskType->find('all')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->DiskType->save($this->request->data)) {
                $this->Flash->success('Марка диска сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->DiskType->save($this->request->data)) {
                $this->Flash->success('Марка диска сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->DiskType->findById($id);
        }
    }

    public function admin_delete($id = null) {
        if ($this->DiskType->delete($id)) {
            $this->Flash->success('Марка диска удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
