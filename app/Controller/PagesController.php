<?php
class PagesController extends AppController {
    public $name = 'Pages';
    public function admin_index() {
        $this->set([
            'pages' => $this->Page->find('all')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->Page->save($this->request->data)) {
                $this->Flash->success('Страница сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->Page->save($this->request->data)) {
                $this->Flash->success('Страница сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->Page->findById($id);
        }
    }

    public function admin_delete($id = null) {
        if ($this->Page->delete($id)) {
            $this->Flash->success('Страница удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function show($id = 1) {
        $this->layout = 'main';
        $page = $this->Page->findById($id);
        $this->set([
            'page' => $page
        ]);
    }

    public function constructor() {
        $this->layout = 'one_col';
    }
}