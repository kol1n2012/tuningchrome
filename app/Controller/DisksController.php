<?php

class DisksController extends AppController {

    public $name = 'Disks';

    public $uses = ['Disk', 'DiskType'];

    public function admin_index() {
        $this->set([
            'disks' => $this->Disk->find('all', [
                'limit' => 20,
                'page' => isset($this->request->query['page']) ? $this->request->query['page'] : 1
            ]),
            'count' => $this->Disk->find('count')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->Disk->save($this->request->data)) {
                $this->Flash->success('Модель машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
        $this->set([
            'disk_types' => $this->DiskType->find('list', [
                'fields' => ['id', 'type']
            ])
        ]);
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->Disk->save($this->request->data)) {
                $this->Flash->success('Модель машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->Disk->findById($id);
        }
        $this->set([
            'disk_types' => $this->DiskType->find('list', [
                'fields' => ['id', 'type']
            ])
        ]);
    }

    public function admin_delete($id = null) {
        if ($this->Disk->delete($id)) {
            $this->Flash->success('Модель машины удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
