<?php

class GalleriesController extends AppController {
    public $name = 'Galleries';
    public function admin_index() {
        $this->set([
            'galleries' => $this->Gallery->find('all', array(
	            'order' => 'Gallery.weight'
            ))
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->Gallery->save($this->request->data)) {
                $this->Flash->success('Галерея сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->Gallery->save($this->request->data)) {
                $this->Flash->success('Галерея сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->Gallery->findById($id);
        }
    }

    public function admin_delete($id = null) {
        if ($this->Gallery->delete($id)) {
            $this->Flash->success('Галерея удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function show($id, $page = 1) {
        $this->layout = 'one_col';
        $this->set([
            'gallery' => $this->Gallery->findById($id),
            'photos' => $this->Gallery->Photo->find('all', array(
            	'conditions' => array(
	            	'gallery_id' => $id,
	            	'published' => 1
            	),
	            'limit' => 12,
	            'page' => $page,
	            'order' => 'Photo.weight'
            )),
            'count' => $this->Gallery->Photo->find('count', array(
	            'conditions' => array(
	            	'gallery_id' => $id,
	            	'published' => 1
            	),
            ))
        ]);
    }
}