<?php

class UsersController extends AppController {

    public $name = 'Users';
    
    public function admin_login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->danger('Неверное имя пользователя или пароль');
            }
        }
    }
    
    public function admin_logout() {
        return $this->redirect($this->Auth->logout());
    }
    
    public function admin_index() {
        $this->set([
        	'users' => $this->User->find('all')
        ]);
    }
    
    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success('Пользователь сохранен');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }
    
    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success('Пользователь сохранен');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->User->findById($id);
        }
    }
    
    public function admin_delete($id = null) {
        if ($this->User->delete($id)) {
            $this->Flash->success('Пользователь удален');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
