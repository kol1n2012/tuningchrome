<?php

class CarTypesController extends AppController {

    public $name = 'CarTypes';

    public function admin_index() {
        $this->set([
            'car_types' => $this->CarType->find('all')
        ]);
    }

    public function admin_add() {
        if (!empty($this->request->data)) {
            if ($this->CarType->save($this->request->data)) {
                $this->Flash->success('Марка машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        }
    }

    public function admin_edit($id = null) {
        if (!empty($this->request->data)) {
            if ($this->CarType->save($this->request->data)) {
                $this->Flash->success('Марка машины сохранена');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->danger('Возникли ошибки при сохранении!');
            }
        } else {
            $this->data = $this->CarType->findById($id);
        }
    }

    public function admin_delete($id = null) {
        if ($this->CarType->delete($id)) {
            $this->Flash->success('Марка машины удалена');
        } else {
            $this->Flash->danger('Возникли ошибки при удалении!');
        }
        $this->redirect(array('action' => 'index'));
    }

}
